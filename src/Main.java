import java.awt.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<String> stringList = new LinkedList<>();
        stringList.add("Jake");
        stringList.add("Fin");
        stringList.add("Jacob");

        System.out.println(stringList.stream()
                .filter(s -> !s.contains("hr"))
                .collect(Collectors.toList()));

        stringList.stream().skip(2).forEach(System.out::println);
        System.out.println();
        stringList.stream().distinct().forEach(System.out::println);
        System.out.println();

        stringList.stream()
                .map(s -> s.replaceAll("o","ttt"))
                .forEach(System.out::println);

        stringList.stream()
                .map(s -> s.toUpperCase())
                .forEach(System.out::println);

        stringList.stream()
                .peek(s -> System.out.println(s));


        stringList.stream()
                .limit(2)
                .sorted();



    }
}
