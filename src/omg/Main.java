package omg;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<String> stringList = new LinkedList<>();
        stringList.add("One");
        stringList.add("Two");
        stringList.add("Three");
        stringList.add("Four");
        stringList.add("Five");
        stringList.add("One");

        System.out.println(stringList.stream()
                .filter(s -> s.contains("One"))
                .count());

        System.out.println(stringList.stream()
                .findFirst()
                .orElse("0"));

        System.out.println(stringList.stream()
                .sorted(String::lastIndexOf)
                .findFirst()
                .get());

        System.out.println(Stream.of()
                .reduce((c, n) -> c == null ? "empty" : n)
                .orElseGet(() -> "empty"));

        System.out.println(stringList.stream()
                .reduce((c, n) -> n)
                .get());

        try {
            System.out.println(stringList.stream()
                    .filter(s -> s.equals("Three"))
                    .findFirst()
                    .orElseThrow(Exception::new)
            );
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(stringList.stream()
                .limit(3)
                .sorted(String::lastIndexOf)
                .findFirst()
                .get());

        stringList.stream()
                .skip(1)
                .limit(2)
                .forEach(System.out::println);

        System.out.println();

        stringList.stream()
                .distinct()
                .forEach(System.out::println);

        System.out.println(stringList.stream()
                .anyMatch(s -> s.contains("One")));

        System.out.println(stringList.stream()
                .allMatch(s -> s.contains("o")));

        List<String> list = stringList.stream()
                .map(s -> s + "_1")
                .collect(Collectors.toList());

        System.out.println(list);
    }

}
