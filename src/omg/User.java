package omg;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class User {

    public static void uhm(){
        List<String> stringList = new LinkedList<>();
        stringList.add("One");
        stringList.add("Two");
        stringList.add("Three");
        stringList.add("Four");
        stringList.add("Five");
        stringList.add("One");

        System.out.println(stringList.stream()
                .filter(s -> s.contains("One"))
                .count());

        System.out.println(stringList.stream()
                .findFirst()
                .orElse("0"));

        System.out.println(stringList.stream()
                .sorted(String::lastIndexOf)
                .findFirst()
                .get());

        System.out.println(Stream.of()
                .reduce((c, n) -> c == null ? "empty" : n)
                .orElseGet(() -> "empty"));

        System.out.println(stringList.stream()
                .reduce((c, n) -> n)
                .get());

        try {
            System.out.println(stringList.stream()
                    .filter(s -> s.equals("Three"))
                    .findFirst()
                    .orElseThrow(Exception::new)
            );
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(stringList.stream()
                .limit(3)
                .sorted(String::lastIndexOf)
                .findFirst()
                .get());

        System.out.println(stringList.stream()
                .skip(2)
                .findFirst()
                .get());
    }



}
